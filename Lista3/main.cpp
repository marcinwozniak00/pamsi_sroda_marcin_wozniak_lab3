#include <iostream>
#include <stos_na_tablicy.h>
#include <stack>
#include <timer.h>
#include <cstdlib>
#include <ctime>

using namespace std;


/**
*Rekurencyjna funckaj ukladajaca wieze hanoi
* Korzysta z 3 stosow prezentujcych paliki
* Pzzestawia z palika 1(zrodlowy) do plaika3(docelowy)
*/
void hanoi(stos_na_tablicy <int> &zrodlowy,stos_na_tablicy <int> &docelowy,int ile ){
    stos_na_tablicy <int> pomocniczy(1);
    /**
    *Poni�szy fragment kodu wyswietla kazdy krok w algorytmie
    *ukladania wiezy hanoi.
    */
/*
    cout<<"----------------------------"<<endl;
    cout<<"__Palik 1__"<<endl;
    zrodlowy.wyswietl();
    cout<<endl;
    cout<<"__Palik 2__"<<endl;
    pomocniczy.wyswietl();
    cout<<endl;
    cout<<"__Palik 3__"<<endl;
    docelowy.wyswietl();
    cout<<endl;
    cout<<"----------------------------"<<endl;
*/

if(ile>1){ //jesli liczba krazkow wieszka niz 1
    hanoi(zrodlowy,pomocniczy,ile-1);
    hanoi(zrodlowy,docelowy,1);
    hanoi(pomocniczy,docelowy,ile-1);
}
else{
    docelowy.push_podwoj(zrodlowy.pop());
}
}


/*Funkcja glowna main*/
int main()
{
    srand( time( NULL ) );
    int a= 1000, b=1000, c=1000; //pomocnicze liczniki do testow
    timer czas1, czas2, czas3; // mierzenie czasu
    stack <int> stosik; //stos z STLa
    stos_na_tablicy <int> stos_1(1), stos_2(1); //stosy na tablicy
    stos_na_tablicy <int> palik_1(1), palik_2(1), palik_3(1); //stosy symboliujace paliki w problemie wiezy hanoi
    int ile; // ilosc krazkow
    int tab[10] = {1,2,3,4,5,6,7,8,9,10}; // tablica z numerami krazkow
    cout<<"Z ile elementow ma skaldac sie wieza? ";
    cin>>ile;
    if(ile>10){ //kontrola danych wejsciowych
    cout<<"Operacja niedozwolona!"<<endl;
    return 0;
    }
    cout<<endl;
    for (int i=0; i<ile; i++) //wypelnianie pomocniczego elementami tablciy
    palik_2.push_podwoj(tab[i]);
    for (int i=0; i<ile; i++) //wypelnianie plaika zrdlowego przez zabieranie elementow z pomocniczego
    palik_1.push_podwoj(palik_2.pop());
    cout<<"Nieposortowana wieza hanoi"<<endl; //wyswietlenie poczatkowej zawarosci stosow
    cout<<" Palik 1: "<<endl;
    palik_1.wyswietl();
    cout<<endl;
    cout<<" Palik 2: "<<endl;
    palik_2.wyswietl();
    cout<<endl;
    cout<<" Palik 3: "<<endl;
    palik_3.wyswietl();
    cout<<endl;


    hanoi(palik_1,palik_3,ile); //wywloanie funcji rekurencyjnej ukladajacej wieze hanoi

    cout<<"************************"<<endl; //wyswietlenie kocowej zawartosci stosow
    cout<<"Posortowana wieza hanoi"<<endl;
    cout<<" Palik 1: "<<endl;
    palik_1.wyswietl();
    cout<<endl;
    cout<<" Palik 2: "<<endl;
    palik_2.wyswietl();
    cout<<endl;
    cout<<" Palik 3: "<<endl;
    palik_3.wyswietl();
    cout<<"************************"<<endl<<endl;


    /**
    *Test czasu dzialania implementacji stosu STL
    */
    cout<<":::::....TESTY.....::::::"<<endl;
    czas1.start();
    while(a>0){
    stosik.push(rand());
    a--;
    }
    czas1.stop();
    cout<<"Czas na STL: "<<czas1.getTime()<<" ms"<<endl;


    /**
    *Test czasu dzialania strategi podwajania
    *stos.push_stala(rand() % 50 )
    *Wypelnia stos losowymi warotscmi, gdy stos pelny powieszka jego rozmiar dwukrotnie
    */
    czas2.start();
    while(b>0){
    stos_1.push_podwoj(rand());
    b--;
    }
    czas2.stop();
    cout<<"Czas strategii podwajania : "<<czas2.getTime()<<" ms"<<endl;


    /**
    *Test czasu dzialania strategi inkrementalnej
    *stos.push_stala(rand() % 50, 100 )
    *Wypelnia stos losowymi warotscmi, gdy stos pelny powieszka jego rozmiar o 100
    */
    czas3.start();
    while(c>0){
    stos_2.push_stala(rand(), 1 );
    c--;
    }
    czas3.stop();
    cout<<"Czas strategii inkremetalnej: "<<czas3.getTime()<<" ms"<<endl;

    return 0;
}

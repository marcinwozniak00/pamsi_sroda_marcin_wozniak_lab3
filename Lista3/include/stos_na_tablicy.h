#ifndef STOS_NA_TABLICY_H
#define STOS_NA_TABLICY_H
#include <iostream>

using namespace std;

/*Klasa definiujaca stos bazujacy na tablicy*/
template <typename typ> class stos_na_tablicy{
private:
    int pojemnosc; //pojemnosc stosu
    typ *S; // tablica stosu
    int top; //g�ra sotsu
public:
    stos_na_tablicy(int p){ //konstruktor
    pojemnosc = p;
    S = new typ[pojemnosc];
    top = -1;
    }
    ~stos_na_tablicy(){cout<<endl;} //destruktor
    bool isEmpty(){
    return (top<0);
    }
    int rozmiar (){
        cout<<"Rozmiar: "<<top+1<<endl;
    return top+1;
    }

    /*Dodawanie elementwo przy strategi podwajania*/
    void push_podwoj (typ o){
        if(top+1>=pojemnosc){ //jesli stos pelny
        pojemnosc = pojemnosc*2; //podowj pojemnosc
        typ* tmp = new typ[pojemnosc];
        for( int i=0; i<top+1;++i)
            tmp[i]=S[i];

            delete[]S; //zwolnienie pamieci
            S=tmp;
        }
        S[++top]=o; //zapisanie elementu do stosu

    }
    /*Dodawanie elementow przy strategi powiekszania o stala*/
     void push_stala (typ o, int powiekszenie){
        if(top+1>=pojemnosc){ //jesli stos pelny
        pojemnosc = pojemnosc+powiekszenie; //zwieksz pojemnosc o zadana wartosc
        typ* tmp = new typ[pojemnosc];
        for( int i=0; i<top+1;++i)
            tmp[i]=S[i];

            delete[]S; //zwolnienie pamiecie
            S=tmp;
        }
        S[++top]=o; //zapsianie elelemntu do stosu

    }

    /*Metoda wyciagajaca element z lsity*/
    typ pop(){
    if(isEmpty()){ //obssluga wyjatkow
    string wyjatek ="Operacja niedozwolona!";
   throw wyjatek;}
    return S[top--];
    }
    /*Metoda wyswietlajaca liste*/
    void wyswietl(){
    if(isEmpty()) //oblsuga wyjatkow
    cout<<endl;
    //cout<<"Zawartosc stosu"<<endl;
    int tmp;
    tmp=top;
    while (tmp>=0){
        cout<<S[tmp]<<endl;
        tmp--;
    }
    }
};

#endif // STOS_NA_TABLICY_H
